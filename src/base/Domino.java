	package base;
	/**
	 * @author Kevan Buckley, maintained by __student
	 * @version 2.0, 2014
	 */
	
	public class Domino implements Comparable<Domino> {
	  public int high;
	  public int low;
	  public int hx;
	  public int hy;
	  public int lx;
	  public int ly;
	  public boolean placed = false;
	
	  public Domino(int high, int low) {
	    super();
	    this.high = high;
	    this.low = low;
	  }
	  
	  public void place(int hx, int hy, int lx, int ly) {
	    this.hx = hx;
	    this.hy = hy;
	    this.lx = lx;
	    this.ly = ly;
	    placed = true;
	  }
	
	  public String toString() {
	    StringBuffer result = new StringBuffer();
	    result.append("[").append(high).append(low).append("]");
	    if(!placed){
	      result.append("unplaced");
	    } else {
	      result.append("(").append(hx+1).append(",").append(hy+1).append(")")
	      .append("(").append(lx+1).append(",").append(ly+1).append(")");
	    }
	    return result.toString();
	  }
	
	  /** turn the domino around 180 degrees clockwise*/
	  
	  public void invert() {
	    int tx = hx;
	    hx = lx;
	    lx = tx;
	    
	    int ty = hy;
	    hy = ly;
	    ly = ty;    
	  }
	
	  public boolean isHorizontal() {    
	    return hy == ly;
	  }
	
	
	  public int compareTo(Domino domino) {
	    if(this.high < domino.high){
	      return 1;
	    }
	    return this.low - domino.low;
	  }
	  
	  
	  
	}
